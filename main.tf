module "dynamodb-table" {
  source                                = "terraform-aws-modules/dynamodb-table/aws"
  version                               = "3.3.0"
  for_each                              = var.dynamodb-tables
  name                                  = each.key
  attributes                            = try(each.value.attributes, var.attributes)
  hash_key                              = try(each.value.hash_key, var.hash_key)
  range_key                             = try(each.value.range_key, var.range_key)
  billing_mode                          = try(each.value.billing_mode, var.billing_mode)
  write_capacity                        = try(each.value.write_capacity, var.write_capacity)
  read_capacity                         = try(each.value.read_capacity, var.read_capacity)
  point_in_time_recovery_enabled        = try(each.value.point_in_time_recovery_enabled, var.point_in_time_recovery_enabled)
  ttl_enabled                           = try(each.value.ttl_enabled, var.ttl_enabled)
  ttl_attribute_name                    = try(each.value.ttl_attribute_name, var.ttl_attribute_name)
  global_secondary_indexes              = try(each.value.global_secondary_indexes, var.global_secondary_indexes)
  local_secondary_indexes               = try(each.value.local_secondary_indexes, var.local_secondary_indexes)
  replica_regions                       = try(each.value.replica_regions, var.replica_regions)
  stream_enabled                        = try(each.value.stream_enabled, var.stream_enabled)
  stream_view_type                      = try(each.value.stream_view_type, var.stream_view_type)
  server_side_encryption_enabled        = try(each.value.server_side_encryption_enabled, var.server_side_encryption_enabled)
  server_side_encryption_kms_key_arn    = try(each.value.server_side_encryption_kms_key_arn, var.server_side_encryption_kms_key_arn)
  tags                                  = try(each.value.tags, var.tags)
  timeouts                              = try(each.value.timeouts, var.timeouts)
  autoscaling_enabled                   = try(each.value.autoscaling_enabled, var.autoscaling_enabled)
  autoscaling_defaults                  = try(each.value.autoscaling_defaults, var.autoscaling_defaults)
  autoscaling_read                      = try(each.value.autoscaling_read, var.autoscaling_read)
  autoscaling_write                     = try(each.value.autoscaling_write, var.autoscaling_write)
  autoscaling_indexes                   = try(each.value.autoscaling_indexes, var.autoscaling_indexes)
  table_class                           = try(each.value.table_class, var.table_class)
  deletion_protection_enabled           = try(each.value.deletion_protection_enabled, var.deletion_protection_enabled)
  ignore_changes_global_secondary_index = try(each.value.ignore_changes_global_secondary_index, var.ignore_changes_global_secondary_index)

}